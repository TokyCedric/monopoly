package com.company;

public class Joueur {
    private String nom;
    private int position;
    private int argent;
    private int nbTours = 0;
    private boolean possedeCartePrison;
    private boolean banqueRoute;
    private int nombreMaison = 0;

    public Joueur(String nom) {
        setNom(nom);
        setPosition(0);
        setArgent(1500);
        this.banqueRoute = false;
        this.setPossedeCartePrison(false);
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        if(nom == null || nom.isEmpty())
            throw new IllegalArgumentException("Le nom ne peux pas etre vide ou null");
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Nom : " + nom + " Argent : "+argent+"$";
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getArgent() {
        return argent;
    }

    public void setArgent(int argent) {
        this.argent = argent;
    }

    public void estBanqueRoute() {
        this.banqueRoute = true;
    }

    public void ajouterArgent(int argent) {
        this.argent += argent;
    }

    public boolean getBanqueRoute() {
        return banqueRoute;
    }

    public void retirerArgent(int argent) {
        this.argent -= argent;
    }

    public void nouveauTours() {
        this.nbTours += 1;
    }

    public int getNbTours() {
        return nbTours;
    }

    public boolean getPossedeCartePrison() {
        return possedeCartePrison;
    }

    public void setPossedeCartePrison(boolean possedeCartePrison) {
        this.possedeCartePrison = possedeCartePrison;
    }

    public void reculerJoueur(int distance){
        this.position -= distance;
    }

    public void ajoutMaison(int nombre){
        this.nombreMaison += nombre;
    }

    public int getNombreMaison(){
        return this.nombreMaison;
    }

    public void avancerJoueur(int position) {
        if(this.position >= 40) {
            this.position = 0;
            ajouterArgent(500);
        }
        else {
            this.position += position;
        }
    }

    public Boolean verificationPayment(int montantApayer) {
        int argentFinal = this.argent - montantApayer;
        return 0 <= argentFinal;
    }


}
