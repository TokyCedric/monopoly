package com.company.Case;

import com.company.Joueur;

public class CaseTerrain extends Case{
    private String couleur;
    private int prix;
    private Joueur proprietaire;
    private String name;
    private boolean esthypotheque;
    private int prixhypotheque;
    private int numero;
    private int loyer;
    private int prixMaison;
    private int nombreMaison;

    public CaseTerrain(int numero, String nom, String couleur, int prix, int loyer, int prixMaison, int prixhypotheque) {
        super(numero, nom);
        this.couleur = couleur;
        this.prix = prix;
        this.loyer = loyer;
        this.prixhypotheque = prixhypotheque;
        this.esthypotheque = false;
        this.proprietaire = null;
        this.prixMaison = prixMaison;
        this.nombreMaison = 0;
    }

    public Joueur getProprietaire() {
        return proprietaire;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setProprietaire(Joueur joueur) {
        proprietaire = joueur;
    }

    public void supprimeProprietaire() {
        proprietaire = null;
    }

    public boolean getEsthypotheque() {
        return esthypotheque;
    }

    public void setEsthypotheque() {
        this.esthypotheque = true;
    }

    public int getPrixhypotheque() {
        return prixhypotheque;
    }

    public int getloyer() {
        if(nombreMaison > 0) {
            int maison = 200*nombreMaison;
            return loyer+maison;
        }
        else {
            return loyer;
        }
    }
    public int getPrix() {
        return prix;
    }

    public int getNombreMaison() {
        return nombreMaison;
    }

    public int getPrixMaison() {
        return prixMaison;
    }

    public void setNombreMaison(int nbMaison) {
        this.nombreMaison = nbMaison;
    }

    public int getPrixTotalMaison(int nombreMaison) {
        if(this.nombreMaison + nombreMaison > 4) {
            System.out.println("Vous ne pouvez pas avoir plus de 4 maison sur un même terrain");
            return 0;
        }
        else {
            return prixMaison * nombreMaison;
        }

    }

}
