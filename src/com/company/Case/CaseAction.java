package com.company.Case;

public class CaseAction extends Case{
    private String type;

    public CaseAction(int numero, String nom, String type) {
        super(numero, nom);
        this.type = type;
    }

    public String getType(){
        return type;
    }
}
