package com.company.Case;

public class Case {
    private int numero;
    private String nom;

    Case(int numero, String nom){
        this.numero = numero;
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public int getNumero() {
        return numero;
    }

    public String toString() {
        return getNom();
    }
}
