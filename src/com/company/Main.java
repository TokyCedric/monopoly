package com.company;
import java.util.Random;

import com.company.Case.Case;
import com.company.Case.CaseAction;
import com.company.Case.CaseTerrain;


import java.util.Scanner;

public class Main {


	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		boolean Gagner = false;
		boolean estValide = false;
		String choix = "";
		System.out.println("-------------Bienvenue-------------");
		System.out.println("1. Jouer au MONOPOLAX \n2. Parama¨tre \n3. Quitter");
		while (estValide == false) {

			choix = console.nextLine();
			if(choix.equals("1") || choix.equals("2") || choix.equals("3")) {
				estValide = true;
			} else {
				System.out.println("Veuillez saisir un nombre entre 1 et 3");
			}
		}
		if (choix.equals("1")) {
			System.out.println("-------------MONOPOLAX-------------");
			System.out.println("Veuillez saisir le nombre de joueur (min:2 max:4):");
			Joueur[] liste = creationJoueur(console.nextInt());
			Plateau plateau = creationPlateau(liste);
			while (Gagner != true) {
				System.out.println("---------------------");
				Joueur currentJoueur = plateau.aLuiDeJouer();
				jouer(currentJoueur, plateau);
				if (plateau.gagnerLaPartie()) {
					System.out.println("--------------PARTIE TERMINEE------------------");
					System.out.println(" Le vainqueur est " + plateau.joueurGagnant().getNom());
					Gagner = true;
				}
			}
		} else if (choix.equals("2")) {
			System.out.println("-------------Parametres-------------");
		} else if (choix.equals("3")) {
			System.exit(0);
		} else {
			System.out.println("Choix non compris");
		}
		console.close();
	}

	public static void jouer(Joueur joueur, Plateau plateau) {
		boolean finirTour = false;
		Des des = new Des();
		int deJeter = 0;
		joueur.nouveauTours();
		Case caseActuelle = null;
		System.out.println(joueur.getNom() + " à toi de jouer (tours " + joueur.getNbTours() + ")");

		int i=0;
		while (finirTour != true || i == 10 ) {
			i++;
			if (deJeter == 0) {
				afficherMenuDe(joueur, plateau, des);
				caseActuelle = plateau.voireCase(joueur.getPosition());
				if(des.isDouble()) {
					System.out.println("Vous avez fait un double");
				}
				else {
					deJeter = 1;
				}
			}

			if (caseActuelle.getClass().getName() == "com.company.Case.CaseAction"
					|| caseActuelle.getClass().getName() == "com.company.Case.CasePrison") {
				System.out.println("Affiche le menu pour les actions");
				finirTour = afficherMenuCaseAction(joueur, plateau, des);
			} else if (caseActuelle.getClass().getName() == "com.company.Case.CaseTerrain") {
				System.out.println("Affiche le menu pour les achats");
				finirTour = afficherMenuAchatTerrain(joueur, plateau, des);
			}
		}
	}

	private static boolean CaseAction(Joueur joueur, Plateau plateau, Des des) {
    	return false;
	}

	public static Joueur[] creationJoueur(int nombreDeJoueur) {
		Scanner console = new Scanner(System.in);
		if (nombreDeJoueur >= 2 && nombreDeJoueur <= 4) {
			Joueur[] ListeJoueur = new Joueur[nombreDeJoueur];
			for (int i = 0; i < nombreDeJoueur; i++) {
				System.out.println("Veuillez saisir le pseudo du joueur n° " + (i + 1) + ":");
				String nomJoueur = console.nextLine();
				Joueur j = new Joueur(nomJoueur);
				ListeJoueur[i] = j;
			}
			return ListeJoueur;
		} else {
			System.out.println("Veuillez saisir le nombre de joueur (minimun 2, maximun 4).");
			return null;
		}
	}

	public static Plateau creationPlateau(Joueur[] joueur) {
		Plateau plateau = new Plateau(joueur);
		return plateau;
	}

	public static void afficherMenuDe(Joueur j, Plateau p, Des d) {
		d.jeterDes();
		AvancerCase(j, d.getTotal(), p);
	}

	public static void AvancerCase(Joueur joueur,int sommeDe, Plateau plateau) {
		joueur.avancerJoueur(sommeDe);
		Case caseActuelle = plateau.voireCase(joueur.getPosition());
		System.out.println("------------------------------\nLes des ont fait " + sommeDe + " vous arrivez sur la case "
				+ caseActuelle.toString() + "\nTu possedes " + joueur.getArgent() + "$");
		if (caseActuelle.getClass().getName() == "com.company.Case.CaseAction") {
		} else if (caseActuelle.getClass().getName() == "com.company.Case.CaseTerrain") {
			String proprietaire;
			if (((CaseTerrain) caseActuelle).getProprietaire() == null) {
				proprietaire = "Personne";
			} else {
				Joueur joueurProprietaire = ((CaseTerrain) caseActuelle).getProprietaire();
				proprietaire = joueurProprietaire.getNom();
			}
			System.out.println(proprietaire + " est le proprietaire");
		}
	}

	public static boolean afficherMenuCaseAction(Joueur joueur, Plateau plateau, Des de) {
		CaseAction caseActuelle = (CaseAction) plateau.voireCase(joueur.getPosition());
		String typeCase = caseActuelle.getType();
		boolean etat = false;
		switch (typeCase){
			case "Communaute":
				etat = caisseDeCommunaute(joueur,plateau);
				break;
			case "Impot":
				etat = impot(joueur,plateau, de);
				break;
			case "Chance":
				etat = chance(joueur,plateau);
				break;
			case "Taxe":
				etat = taxe(joueur,plateau,de);
				break;
			case "Prison":
				etat = prison(joueur);
				break;
			default:
				break;
		}
		return etat;
	}

	private static boolean prison(Joueur joueur) {
    	System.out.println("Vous allez directement en Prison.");
    	joueur.setPosition(10);
		System.out.println("Votre tour est termine");
		return true;
	}

	private static boolean caisseDeCommunaute(Joueur joueur,Plateau plateau) {
    	Random random = new Random();
    	int carte = random.nextInt(15)+1;
		actionChance(joueur, plateau, carte);
		System.out.println(joueur.toString());
		System.out.println("Votre tour est termine");
		return true;
	}

	private static boolean chance(Joueur joueur,Plateau plateau) {
		Random random = new Random();
		int carte = random.nextInt(15)+1;
		actionChance(joueur, plateau, carte);
		System.out.println(joueur.toString());
		System.out.println("Votre tour est termine");
		return true;
	}

	private static void actionChance(Joueur joueur,Plateau plateau, int carte) {
    	switch (carte){
			case 0:
				retourCaseDepart(joueur);
				break;
			case 1:
				goAvenueHenryM(joueur);
				break;
			case 2:
				goBoulevardVillette(joueur);
				break;
			case 3:
				earnMoney(joueur);
				break;
			case 4:
				payMoney(joueur);
				break;
			case 5:
				getSortiPrison(joueur);
				break;
			case 6:
				reculer(joueur);
				break;
			case 7:
				prison(joueur);
				break;
			case 8:
				payerReparation(joueur);
				break;
			case 9:
				payMoney(joueur);
				break;
			case 10:
				goGare(joueur);
				break;
			case 11:
				goRueDeLaPaix(joueur);
				break;
			case 12:
				payerJoueur(joueur,plateau);
				break;
			case 13:
				getRent(joueur);
				break;
			case 14:
				wonCompetition(joueur);
				break;
			case 15:
				injury(joueur);
				break;
			default:
				break;
		}
	}

	private static void injury(Joueur joueur) {
		System.out.println("Vous avez fait un accident de parachute, payez 200$ de frais d'hôpital\n");
		joueur.retirerArgent(200);
	}

	private static void wonCompetition(Joueur joueur) {
    	System.out.println("Vous avez gagne une competition de kung fu, recevez 150$\n");
    	joueur.ajouterArgent(150);
    }

	private static void getRent(Joueur joueur) {
		System.out.println("Vous recevez de l'argent de la banque car vos maisons ont pris de la valeur");
    	if(joueur.getNombreMaison() > 0) {
			System.out.println("vous avez : " + joueur.getNombreMaison() );
			System.out.println("Vous recevez 20$/maison\n");
			joueur.ajouterArgent(20 * joueur.getNombreMaison());
		}else{
			System.out.println("Dommage vous n'avez pas de maison :'( \n");
			System.out.println("Vous recevez 0$\n");
		}
	}

	private static void payerJoueur(Joueur joueur, Plateau plateau) {
    	System.out.println("Tu as pioche une carte vide\n");
	}

	private static void goRueDeLaPaix(Joueur joueur) {
		System.out.println("Vous allez à la rue de la Paix.");
		joueur.setPosition(39);
		System.out.println("Votre tour est termine");
	}

	private static void goGare(Joueur joueur) {
		while(joueur.getPosition() != 5){
			joueur.avancerJoueur(1);
		}
		System.out.println("Vous etes arrive à la gare Montparnassse\n");
	}

	private static void payerReparation(Joueur joueur) {
    	System.out.println("Vous devez payez les reparations pour chaques maisons.\n");
    	System.out.println("Vous devez payez 25$/maison");
    	int nombreMaison = joueur.getNombreMaison();
    	int montantTotal = 25 * nombreMaison;
		if(joueur.verificationPayment(montantTotal)) {
			joueur.retirerArgent(montantTotal);
			System.out.println("Vous avez paye" + montantTotal + "de reparation\n");
			System.out.println(joueur.toString());
			System.out.println("Votre tour est termine");
		}else{
			System.out.println("Vous n'avez pas assez d'argent, vous avez fait faillite.");
			joueur.estBanqueRoute();
		}
	}

	private static void reculer(Joueur joueur) {
		System.out.println("Vous reculez de 3 cases\n");
		joueur.reculerJoueur(3);
	}

	private static void getSortiPrison(Joueur joueur) {
    	joueur.setPossedeCartePrison(true);
	}

	private static void payMoney(Joueur joueur) {
		System.out.println("Pas de chance, vous devez payez 100$ à la banque\n");
		joueur.retirerArgent(100);
	}

	private static void earnMoney(Joueur joueur) {
    	System.out.println("Vous avez gagner 100$\n");
    	joueur.ajouterArgent(100);
	}

	private static void goBoulevardVillette(Joueur joueur) {
		while(joueur.getPosition() != 11){
			joueur.avancerJoueur(1);
		}
		System.out.println("Vous etes arrive au Boulevard de la Villette\n");
	}

	private static void goAvenueHenryM(Joueur joueur) {
    	while(joueur.getPosition() != 24){
    		joueur.avancerJoueur(1);
		}
    	System.out.println("Vous etes arrive à l'Avenue Henr-Martin\n");
	}

	private static void retourCaseDepart(Joueur joueur) {
    	System.out.println("Vous allez directement à la case depart, vous recevez 500$\n");
		joueur.setPosition(10);
	}

	private static boolean impot(Joueur joueur, Plateau plateau, Des de) {
		if(joueur.verificationPayment(200)) {
			joueur.retirerArgent(200);
			System.out.println("Vous avez paye 200$ d'impôts sur le revenu\n");
			System.out.println(joueur.toString());
			System.out.println("Votre tour est termine");
			return true;
		}else{
			System.out.println("Vous n'avez pas assez d'argent, vous avez fait faillite.");
			joueur.estBanqueRoute();
			return true;
		}

	}

	private static boolean taxe(Joueur joueur, Plateau plateau, Des de) {
		if(joueur.verificationPayment(100)) {
			joueur.retirerArgent(200);
			System.out.println("Vous avez paye 100$ de taxe de luxe\n");
			System.out.println(joueur.toString());
			System.out.println("Votre tour est termine");
			return true;
		}else{
			System.out.println("Vous n'avez pas assez d'argent, vous avez fait faillite.");
			joueur.estBanqueRoute();
			return true;
		}
	}

	public static boolean afficherMenuAchatTerrain(Joueur joueur, Plateau plateau, Des de) {
		Scanner console = new Scanner(System.in);
		CaseTerrain caseActuelle = (CaseTerrain) plateau.voireCase(joueur.getPosition());
		if (caseActuelle.getProprietaire() == joueur && caseActuelle.getCouleur() != "Gare"
				&& caseActuelle.getCouleur() != "Compagnie") {
			System.out.println("1. Acheter une/des maison(s) \n2. Stats \n3. Finir son tour");
			int nbMenu = console.nextInt();
			if (nbMenu == 1) {
				acheterMaison(joueur, plateau);
			} else if (nbMenu == 2) {
				System.out.println("Vous etes à la case : " + caseActuelle.toString() + "\n");
				System.out.println(joueur.toString() + "\nVos proprietes :\n" + plateau.afficherSesProprietes(joueur));
			} else if (nbMenu == 3) {
				if (de.isDouble()) {
					return false;
				} else {
					plateau.joueurSuivant();
					return true;
				}
			} else {
				System.out.println("Entrez un numero du menu.");
			}
		} else if (caseActuelle.getProprietaire() == null) {
			System.out.println(
					"1. Acheter le terrain pour " + caseActuelle.getPrix() + "$\n2. Stats \n3. Finir son tour");
			int nbMenu = console.nextInt();
			if (nbMenu == 1) {
				acheterProprieter(joueur, plateau);
			} else if (nbMenu == 2) {
				System.out.println(joueur.toString() + "\nVos proprietes :\n" + plateau.afficherSesProprietes(joueur));
			} else if (nbMenu == 3) {
				if (de.isDouble()) {
					return false;
				} else {
					plateau.joueurSuivant();
					return true;
				}
			} else {
				System.out.println("Entrez un numero du menu.");
			}
		} else if (caseActuelle.getProprietaire() == joueur && (caseActuelle.getCouleur() == "Gare" || caseActuelle.getCouleur() == "Compagnie")) {
			System.out.println("\n1. Stats \n2. Finir son tour");
			int nbMenu = console.nextInt();
			if (nbMenu == 1) {
				System.out.println(joueur.toString() + "\nVos proprietes :\n" + plateau.afficherSesProprietes(joueur));
			} else if (nbMenu == 2) {
				if (de.isDouble()) {
					return false;
				} else {
					plateau.joueurSuivant();
					return true;
				}
			} else {
				System.out.println("Entrez un numero du menu.");
			}
		} else {
			System.out.println("1. Payer son du \n2. Stats \n3. Finir son tour");
			int nbMenu = console.nextInt();
			if (nbMenu == 1) {
				payerSonDue(joueur, plateau);
				return true;
			} else if (nbMenu == 2) {
				System.out.println(joueur.toString() + "\nVos proprietes :\n" + plateau.afficherSesProprietes(joueur));
			} else if (nbMenu == 3) {
				if (de.isDouble()) {
					return false;
				} else {
					plateau.joueurSuivant();
					return true;
				}
			} else {
				System.out.println("Entrez un numero du menu.");
			}
		}
		return false;
	}

	public static void payerSonDue(Joueur j, Plateau p) {
		CaseTerrain caseActuelle = (CaseTerrain) p.voireCase(j.getPosition());
		Joueur joueurProprietaire = caseActuelle.getProprietaire();
		p.payerSonDu(joueurProprietaire, j, caseActuelle);

	}

	public static void acheterProprieter(Joueur j, Plateau p) {
		Case caseActuelle = p.voireCase(j.getPosition());
		((CaseTerrain) caseActuelle).setProprietaire(j);
		if (j.verificationPayment(((CaseTerrain) caseActuelle).getPrix())) {
			j.retirerArgent(((CaseTerrain) caseActuelle).getPrix());
			System.out.println("Vous avez achete la propriete " + caseActuelle + " pour "
					+ ((CaseTerrain) caseActuelle).getPrix() + "$");
		} else {
			System.out.println("Vous n'avez pas assez d'argent");
		}
	}

	public static void acheterMaison(Joueur joueur, Plateau p) {
		Scanner console = new Scanner(System.in);
		CaseTerrain caseActuelle = (CaseTerrain) p.voireCase(joueur.getPosition());
		System.out.println("Vous posseder " + caseActuelle.getNombreMaison() + " maison(s)");
		System.out.println("Combien de maison souhaiter vous acheter (" + caseActuelle.getPrixMaison()
				+ "$) ? (max 4 sur un terrain)");
		int nbMaisonSouhaite = console.nextInt();
		if (caseActuelle.getPrixTotalMaison(nbMaisonSouhaite) != 0) {

			if (joueur.verificationPayment(caseActuelle.getPrixTotalMaison(nbMaisonSouhaite))) {
				joueur.retirerArgent(caseActuelle.getPrixTotalMaison(nbMaisonSouhaite));
				caseActuelle.setNombreMaison(nbMaisonSouhaite);
				joueur.ajoutMaison(nbMaisonSouhaite);
				System.out.println("Vous avez achete " + nbMaisonSouhaite + " maison(s)");
			} else {
				System.out.println("Vous n'avez pas assez d'argent");
			}
		}else{
			System.out.println("Aucune maison achete");
		}
	}
}