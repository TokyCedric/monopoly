package com.company.Carte;

import com.company.Joueur;

public class CarteTransaction extends Carte{
    private int montantTransaction;
    private Joueur destinataire;


    public CarteTransaction(int type, String contenu) {
        super(type, contenu);
    }

    public CarteTransaction(int type, String contenu, int montantTransaction, Joueur destinataire) {
        super(type, contenu);
        this.montantTransaction = montantTransaction;
        this.destinataire = destinataire;
    }
}
