package com.company.Carte;

import com.company.Joueur;

public class CarteSortiPrison extends Carte{
    private Joueur proprietaire;
    private boolean estUtilisee;

    public CarteSortiPrison(int type, String contenu, Joueur proprietaire, boolean estUtilisee) {
        super(type, contenu);
        this.proprietaire = proprietaire;
        this.estUtilisee = estUtilisee;
    }
}
