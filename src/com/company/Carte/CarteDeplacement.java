package com.company.Carte;

public class CarteDeplacement extends Carte{
    int caseDestination;

    public CarteDeplacement(int type, String contenu,int caseDestination) {
        super(type, contenu);
        this.caseDestination = caseDestination;
    }
}
