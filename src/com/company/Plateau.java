package com.company;
import com.company.Case.Case;
import com.company.Case.CaseAction;
import com.company.Case.CasePrison;
import com.company.Case.CaseTerrain;

import java.util.Scanner;


public class Plateau {

    private Joueur[] joueurs;
    private Case[] cases;
    private Joueur currentJoueur;
    private int indexJoueur;

    public Plateau(Joueur[] joueur) {
        this.joueurs = joueur;
        this.currentJoueur = joueur[0];
        this.indexJoueur = 0;

        // modifier la taille de Case[23] si tu rajoute des ligne
        cases = new Case[40];
        Case c0 = new CaseAction(0, "Case depart", "Depart");
        Case c1 = new CaseTerrain(1, "Boulevard de Belleville", "Marron", 60, 2, 50, 30);
        Case c2 = new CaseAction(2, "Caisse de communaute", "Communaute");
        Case c3 = new CaseTerrain(3, "Rue Lecourbe", "Marron", 60, 4, 50, 30);
        Case c4 = new CaseAction(4, "Impôts sur le revenu", "Impot");
        Case c5 = new CaseTerrain(5, "Gare Montparnasse", "Gare", 200, 25, 0, 100);
        Case c6 = new CaseTerrain(6, "Rue de Vaugirard", "Bleu claire", 100, 6, 50, 50);
        Case c7 = new CaseAction(7, "Chance", "Chance");
        Case c8 = new CaseTerrain(8, "Rue de Courcelles", "Bleu claire", 100, 6, 50, 50);
        Case c9 = new CaseTerrain(9, "Avenue de la Republique", "Bleu claire", 120, 8, 50, 60);
        Case c10 = new CasePrison(10, "Visite Prison");
        Case c11 = new CaseTerrain(11, "Boulevard de la Villette", "Violet", 140, 10, 100, 70);
        Case c12 = new CaseTerrain(12, "Compagnie de disptribution d'electricite", "Compagnie", 150, 0, 0, 75);
        Case c13 = new CaseTerrain(13, "Avenue de Neuilly", "Violet", 140, 10, 100, 70);
        Case c14 = new CaseTerrain(14, "Rue de Paradis", "Violet", 160, 12, 100, 80);
        Case c15 = new CaseTerrain(15, "Gare De Lyon", "Gare", 200, 25, 0, 100);
        Case c16 = new CaseTerrain(16, "Avenue Mozart", "Orange", 180, 14, 100, 90);
        Case c17 = new CaseAction(17, "Caisse de communaute", "Communaute");
        Case c18 = new CaseTerrain(18, "Boulevard Saint-Michel", "Orange", 180, 14, 100, 90);
        Case c19 = new CaseTerrain(19, "Place Pigalle", "Orange", 200, 16, 100, 100);
        Case c20 = new CaseAction(20, "Parc Gratuit", "Parc");
        Case c21 = new CaseTerrain(21, "Avenue Matignon", "Rouge", 220, 18, 150, 110);
        Case c22 = new CaseAction(22, "Chance", "Chance");
        Case c23 = new CaseTerrain(23, "Boulevard Malesherbes", "Rouge", 220, 18, 150, 110);
        Case c24 = new CaseTerrain(24, "Avenue Henri-Martin", "Rouge", 240, 20, 150, 120);
        Case c25 = new CaseTerrain(25, "Gare Du Nord", "Gare", 200, 25, 0, 100);
        Case c26 = new CaseTerrain(26, "Faubourg Saint-Honore", "Jaune", 260, 22, 150, 130);
        Case c27 = new CaseTerrain(27, "Place de la Bourse", "Jaune", 260, 22, 150, 130);
        Case c28 = new CaseTerrain(28, "Compagnie de disptribution des eaux", "Compagnie", 150, 0, 0, 75);
        Case c29 = new CaseTerrain(29, "Rue La Fayette", "Jaune", 280, 24, 150, 140);
        Case c30 = new CaseAction(30, "Allez en Prison","Prison");
        Case c31 = new CaseTerrain(31, "Avenue de Breteuil", "Vert", 300, 26, 200, 150);
        Case c32 = new CaseTerrain(32, "Avenue Foch", "Vert", 300, 26, 200, 150);
        Case c33 = new CaseAction(33, "Caisse de communaute", "Communaute");
        Case c34 = new CaseTerrain(34, "Boulevard des Capucines", "Vert", 320, 28, 200, 160);
        Case c35 = new CaseTerrain(35, "Gare De Saint-Lazare", "Gare", 200, 25, 0, 100);
        Case c36 = new CaseAction(36, "Chance", "Chance");
        Case c37 = new CaseTerrain(37, "Avenue des Champs-Elysees", "Bleu", 350, 35, 200, 175);
        Case c38 = new CaseAction(38, "Taxe de luxe", "Taxe");
        Case c39 = new CaseTerrain(39, "Rue de la Paix", "Bleu", 400, 50, 200, 200);

        cases[0] = c0;
        cases[1] = c1;
        cases[2] = c2;
        cases[3] = c3;
        cases[4] = c4;
        cases[5] = c5;
        cases[6] = c6;
        cases[7] = c7;
        cases[8] = c8;
        cases[9] = c9;
        cases[10] = c10;
        cases[11] = c11;
        cases[12] = c12;
        cases[13] = c13;
        cases[14] = c14;
        cases[15] = c15;
        cases[16] = c16;
        cases[17] = c17;
        cases[18] = c18;
        cases[19] = c19;
        cases[20] = c20;
        cases[21] = c21;
        cases[22] = c22;
        cases[23] = c23;
        cases[24] = c24;
        cases[25] = c25;
        cases[26] = c26;
        cases[27] = c27;
        cases[28] = c28;
        cases[29] = c29;
        cases[30] = c30;
        cases[31] = c31;
        cases[32] = c32;
        cases[33] = c33;
        cases[34] = c34;
        cases[35] = c35;
        cases[36] = c36;
        cases[37] = c37;
        cases[38] = c38;
        cases[39] = c39;
    }

    public Case voireCase(int index) {
        Case c;
        if (index >= 39) {
            c = cases[index-39];
        } else {
            c = cases[index];
        }
        return c;
    }

    public Joueur aLuiDeJouer() {
        return currentJoueur;
    }

    public void joueurSuivant() {
        if (indexJoueur >= joueurs.length - 1) {
            indexJoueur = 0;
            currentJoueur = joueurs[indexJoueur];
        } else {
            indexJoueur += 1;
            currentJoueur = joueurs[indexJoueur];
        }
    }

    public String afficherSesProprietes(Joueur joueurActuelle) {
        String casesProprietaire = "";
        CaseTerrain c = null;
        int y = 0;
        for (int i = 0; i < this.cases.length; i++) {
            if (cases[i].getClass().getName() == "com.company.Case.CaseTerrain") {
                c = (CaseTerrain) cases[i];
                if (c.getProprietaire() == joueurActuelle) {
                    casesProprietaire += c.getNom() + " (" + c.getNombreMaison() + " maison(s))\n";
                    y++;
                }
            }
        }
        if (y > 0) {
            return casesProprietaire;
        } else {
            return "Tu n'a pas de propriete";
        }
    }

    public int afficherNombreProprieterPossede(Joueur joueurActuelle) {
        int y = 0;
        CaseTerrain c = null;
        for (int i = 0; i < cases.length; i++) {
            if (cases[i].getClass().getName() == "esgi.Case.CaseTerrain") {
                c = (CaseTerrain) cases[i];
                if (c.getProprietaire() == joueurActuelle) {
                    y++;
                }
            }
        }
        return y;
    }

    public void payerSonDu(Joueur joueurProprietaire, Joueur joueurVisiteur, Case caseActuelle) {
        System.out.println("Vous devez payer: "+((CaseTerrain) caseActuelle).getloyer()+"!");
        int prixVisite = ((CaseTerrain) caseActuelle).getloyer();
        if (joueurVisiteur.verificationPayment(prixVisite)) {
            joueurProprietaire.ajouterArgent(prixVisite);
            joueurVisiteur.retirerArgent(prixVisite);
        } else {
            if (afficherNombreProprieterPossede(joueurVisiteur) == 0) {
                System.out.println(joueurVisiteur.getNom() + " a fait banqueroute");
                joueurVisiteur.estBanqueRoute();
            } else {
                hypothequeTerrain(joueurVisiteur);
                payerSonDu(joueurProprietaire, joueurVisiteur, caseActuelle);
            }
        }

    }

    public void hypothequeTerrain(Joueur joueur) {
        Scanner console = new Scanner(System.in);
        String casesProprietaire = "";
        CaseTerrain c = null;
        for (int i = 0; i < this.cases.length; i++) {
            if (cases[i].getClass().getName() == "com.company.Case.CaseTerrain") {
                c = (CaseTerrain) cases[i];
                if (c.getProprietaire() == joueur) {
                    casesProprietaire += c.getNumero() + " " + c.getNom() + " valeur hypotheque:"
                            + c.getPrixhypotheque() + "�\n";
                }
            }
        }
        System.out.println(casesProprietaire + "\nLaquelle voulez-vous vendre");
        int reponce = console.nextInt();
        c = (CaseTerrain) cases[reponce];
        c.setProprietaire(null);
        joueur.ajouterArgent(c.getPrixhypotheque());
        System.out.println("Vous avez vendu " + c.getNom() + " au prix de " + c.getPrixhypotheque() + "�");
    }

    public boolean gagnerLaPartie() {
        int nbJoueurBanqueRoute = 0;
        for(int i=0; i<joueurs.length; i++) {
            if(joueurs[i].getBanqueRoute()) {
                nbJoueurBanqueRoute++;
            }
        }
        if(nbJoueurBanqueRoute >= joueurs.length-1) {
            return true;
        }
        else {
            return false;
        }
    }

    public Joueur joueurGagnant() {
        Joueur j = null;
        for(int i=0; i<joueurs.length; i++) {
            if(joueurs[i].getBanqueRoute() == false) {
                j = joueurs[i];
            }
        }
        return j;
    }





}
