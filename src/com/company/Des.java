package com.company;
import java.util.Random;

public class Des {

    int de1;
    int de2;

    public Des() {
        de1 = 0;
        de2 = 0;
    }

    public void jeterDes() {
        Random random = new Random();
        de1 = random.nextInt(5) + 1;
        de2 = random.nextInt(5) + 1;
    }

    public int getTotal() {
       // return 7;
        return de1 + de2;
    }

    public int getDe1() {
        return de1;
    }

    public int getDe2() {
        return de2;
    }

    public boolean isDouble() {
        return de1 == de2;
    }

}
